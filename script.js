"use strict";
const select = document.querySelector("#myselect");
const table = document.querySelector("#mytable");
const button = document.querySelector("#calculate");
select === null || select === void 0 ? void 0 : select.addEventListener("change", e => {
    e.preventDefault();
    if ((table === null || table === void 0 ? void 0 : table.rows) == null)
        return;
    table.tBodies[0].remove();
    let value = select.value;
    let tbody = table.createTBody();
    for (let i = 0; i < value; i++) {
        var row = tbody.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var input = document.createElement("input");
        input.setAttribute("id", "input" + i);
        cell1.append(input);
    }
});
button === null || button === void 0 ? void 0 : button.addEventListener("click", e => {
    e.preventDefault();
    if ((table === null || table === void 0 ? void 0 : table.rows) == null)
        return;
    for (let i = 0; i < table.rows.length - 1; i++) {
        const inputvalue = document.querySelector("#input" + i);
        let num = inputvalue === null || inputvalue === void 0 ? void 0 : inputvalue.value;
        if ((inputvalue === null || inputvalue === void 0 ? void 0 : inputvalue.value) == "") {
            let rownum = i + 1;
            alert("No number entered in row " + rownum + ".");
            var cell2 = table.rows[i + 1].cells[1];
            cell2.innerHTML = "";
        }
        else {
            var cell2 = table.rows[i + 1].cells[1];
            cell2.innerHTML = num * num * num;
        }
    }
});
